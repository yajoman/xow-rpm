# RPM package for xow

[xow](https://medusalix.github.io/xow/) is a Linux driver for the Xbox One wireless
dongle.

[It cannot be freely distributed due to licensing problems with Microsoft](https://github.com/medusalix/xow/tree/73f5273e6fda99cc221d0d8d4806f5a59a8ce29d#important-notes),
so I'm unable to provide an RPM repository legally.

However, that doesn't mean I cannot provide you with the .spec file, so you can build
the RPM yourself.

This is specially useful for rpm-ostree distros such as Fedora Silverblue.

## How to build

### Fedora

If you're on Fedora Silverblue:

```bash
toolbox enter
```

Then, to build the package:

```bash
git clone https://gitlab.com/yajoman/xow-rpm.git
cd xow-rpm
sudo dnf builddep xow.spec
fedpkg local
```

After that, you will find the RPM package in a folder named after you CPU arch. For
example, if your arch is `x86_64`, the package will be in the `./x86_64/` folder.

⚠️ BEWARE! The `*.src.rpm` package is **not** what you want to install.

[More docs here](https://docs.fedoraproject.org/en-US/quick-docs/creating-rpm-packages/#preparing-your-system-to-create-rpm-packages).

## How to install

Once you built the package, now just install it with your package manager:

```bash
sudo dnf install ./$(uname -i)/xow-0.5-1.fc33.$(uname -i).rpm
```

Or, on Fedora Silverblue, replace `dnf` for `rpm-ostree`.
